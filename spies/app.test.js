const expect = require('expect');
const rewire = require('rewire');

//Rewire is used like require
let app = rewire('./app');
//app.__set__ and app.__get__ are used

describe('App', () => {

    //Simple test case with spies
    it('Should call a spy correctly', () => {
        let spy = expect.createSpy();
        spy('Omar', 31);
        // expect(spy).toHaveBeenCalled();
        expect(spy).toHaveBeenCalledWith('Omar', 31);
    });

    //Test Case with spy and rewire to test functions ran in app.js

    //Create our spy to replace db required in app.js
    let db = {
        saveUser: expect.createSpy()
    };
    //Set app to use the spy as the db
    app.__set__('db', db);

    it('Should call saveUser with user object', () => {
        let email = 'tanti.omar@gmail.com';
        let password = '123abc';

        app.handleSignup(email, password);
        expect(db.saveUser).toHaveBeenCalledWith({email, password});
    });

});