const request = require('supertest');
const expect = require('expect');

const app = require('./server').app;

describe('Server', () => {

    describe('GET /', () => {

        it('should return {error: "Page not found"} response', (done) => {
            request(app)
                .get('/')
                .expect('Content-type', 'application/json; charset=utf-8')
                .expect(404)
                .expect((res) => {
                    expect(res.body).toInclude({name: 'Todo App v1.0'});
                })
                .end(done);
        });

    });

    describe('GET /users', () => {

        it('should include a user with name "Omar" and age "31"', (done) => {
            request(app)
                .get('/users')
                .expect(200)
                .expect((res) => {
                    expect(res.body).toInclude({
                        name: 'Omar',
                        age: 31
                    });
                })
                .end(done);
        });

    });

});