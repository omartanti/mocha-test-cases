const express = require('express');

var app = express();

app.get('/', (req, res) => {
    res.status(404).send({
        error: 'Page not found!',
        name: 'Todo App v1.0'
    });
});

app.get('/users', (req, res) => {
    res.send([
        {
            name: 'Omar',
            age: 31
        },
        {
            name: 'Dona',
            age: 31
        },
        {
            name: 'Gabrijel',
            age: 1
        }
    ]);
});

app.listen(3000);

module.exports.app = app;