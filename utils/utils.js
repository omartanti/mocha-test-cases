// module.exports.add = (a,b) => a + b + 5; //This will fail the test
// module.exports.square = (x) => x * x + 1; //This will fail the test

module.exports.add = (a,b) => a + b; //This will pass the test

module.exports.asyncAdd = (a,b, callback) => {
    setTimeout(() => {
        return callback(a + b);
    }, 1000);
};

module.exports.square = (x) => x * x; //This will pass the test

module.exports.asyncSquare = (x, callback) => {
    setTimeout(() => {
        return callback(x * x);
    }, 1000);
};


module.exports.setName = (user, fullName) => {
    let names = fullName.split(' ');
    user.firstName = names[0];
    user.lastName = names[1];
    return user;
};