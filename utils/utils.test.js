const expect = require('expect');

const utils = require('./utils');

//Describe will group the tests together for better rendering in terminal
describe('Utils', () => {

    describe('#add', () => {

        it('it should add two numbers', () => {
            let result = utils.add(2, 5);

            expect(result)
                .toBeA('number')
                .toBe(7, `Expected 7, but got ${result}`);
        });

        //For Async Tests we need to pass the 'done' parameter in our it callback
        it('it should async add two numbers', (done) => {
            utils.asyncAdd(3, 4, (sum) => {
                expect(sum).toBe(7).toBeA('number');
                done(); //call 'done();' when async call is finished
            });
        });

    });

    describe('#square', () => {

        it('it should square a number', () => {
            let result = utils.square(5);

            expect(result)
                .toBeA('number')
                .toBe(25, `Expected 25, but got ${result}`);
        });

        //For Async Tests we need to pass the 'done' parameter in our it callback
        it('it should async square a numbers', (done) => {
            utils.asyncSquare(5, (result) => {
                expect(result).toBe(25).toBeA('number');
                done(); //call 'done();' when async call is finished
            });
        });

    });

    //For Documentation Purposes
    it('should expect some values', () => {
        //toBe and toNotBe works fins with numbers and strings
        expect(12).toBe(12);

        //toBe and toNotBe does not work for arrays/object
        expect({name: 'Omar'}).toEqual({name: 'Omar'});

        //toInclude and toExclude work with arrays and objects
        expect([1, 2, 3, 4]).toInclude(2);
        expect([1, 2, 3, 4]).toExclude(5);
        expect({
            name: 'Omar',
            age: 31,
            location: 'Fgura'
        }).toInclude({
            age: 31
        });
    });

});

it('should verify first and last name are set', () => {
    var user = {
        age: 31,
        location: 'Fgura'
    };
    utils.setName(user, 'Omar Tanti');
    expect(user).toInclude({firstName: 'Omar', lastName: 'Tanti'});
});